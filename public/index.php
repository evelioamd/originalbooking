<?php

ini_set('display_errors', 0);

require_once __DIR__ . '/../vendor/autoload.php';

$app = require __DIR__ . '/../src/app.php';
if (getenv('APP_DEV') == 'development') {
	require __DIR__ . '/../config/dev.php';
} else {
	require __DIR__ . '/../config/prod.php';
}
require __DIR__ . '/../src/services.php';
require __DIR__ . '/../src/routes.php';
$app->run();
