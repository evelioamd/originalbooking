-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: originalbooking
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `ViewRooms`
--

DROP TABLE IF EXISTS `ViewRooms`;
/*!50001 DROP VIEW IF EXISTS `ViewRooms`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ViewRooms` AS SELECT 
 1 AS `id`,
 1 AS `adults`,
 1 AS `childrens`,
 1 AS `category`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ViewRoomsDetails`
--

DROP TABLE IF EXISTS `ViewRoomsDetails`;
/*!50001 DROP VIEW IF EXISTS `ViewRoomsDetails`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ViewRoomsDetails` AS SELECT 
 1 AS `field`,
 1 AS `lang`,
 1 AS `value`,
 1 AS `foreign_id`,
 1 AS `model`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `alpha_2_code` varchar(10) NOT NULL,
  `alpha_3_code` varchar(10) NOT NULL,
  `numeric_code` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan','AF','AFG',4),(2,'Åland Islands','AX','AFG',248),(3,'Albania','AL','AFG',8),(4,'Algeria','DZ','AFG',12),(5,'American Samoa','AS','AFG',16),(6,'Andorra','AD','AFG',20),(7,'Angola','AO','AFG',24),(8,'Anguilla','AI','AFG',660),(9,'Antarctica','AQ','AFG',10),(10,'Antigua and Barbuda','AG','AFG',28),(11,'Argentina','AR','AFG',32),(12,'Armenia','AM','AFG',51),(13,'Aruba','AW','AFG',533),(14,'Australia','AU','AFG',36),(15,'Austria','AT','AFG',40),(16,'Azerbaijan','AZ','AFG',31),(17,'Bahamas (the)','BS','AFG',44),(18,'Bahrain','BH','AFG',48),(19,'Bangladesh','BD','AFG',50),(20,'Barbados','BB','AFG',52),(21,'Belarus','BY','AFG',112),(22,'Belgium','BE','AFG',56),(23,'Belize','BZ','AFG',84),(24,'Benin','BJ','AFG',204),(25,'Bermuda','BM','AFG',60),(26,'Bhutan','BT','AFG',64),(27,'Bolivia (Plurinational State of)','BO','AFG',68),(28,'Bonaire, Sint Eustatius and Saba','BQ','AFG',535),(29,'Bosnia and Herzegovina','BA','AFG',70),(30,'Botswana','BW','AFG',72),(31,'Bouvet Island','BV','AFG',74),(32,'Brazil','BR','AFG',76),(33,'British Indian Ocean Territory (the)','IO','AFG',86),(34,'Brunei Darussalam','BN','AFG',96),(35,'Bulgaria','BG','AFG',100),(36,'Burkina Faso','BF','AFG',854),(37,'Burundi','BI','AFG',108),(38,'Cabo Verde','CV','AFG',132),(39,'Cambodia','KH','AFG',116),(40,'Cameroon','CM','AFG',120),(41,'Canada','CA','AFG',124),(42,'Cayman Islands (the)','KY','AFG',136),(43,'Central African Republic (the)','CF','AFG',140),(44,'Chad','TD','AFG',148),(45,'Chile','CL','AFG',152),(46,'China','CN','AFG',156),(47,'Christmas Island','CX','AFG',162),(48,'Cocos (Keeling) Islands (the)','CC','AFG',166),(49,'Colombia','CO','AFG',170),(50,'Comoros (the)','KM','AFG',174),(51,'Congo (the Democratic Republic of the)','CD','AFG',180),(52,'Congo (the)','CG','AFG',178),(53,'Cook Islands (the)','CK','AFG',184),(54,'Costa Rica','CR','AFG',188),(55,'Côte d\'Ivoire','CI','AFG',384),(56,'Croatia','HR','AFG',191),(57,'Cuba','CU','AFG',192),(58,'Curaçao','CW','AFG',531),(59,'Cyprus','CY','AFG',196),(60,'Czech Republic (the)','CZ','AFG',203),(61,'Denmark','DK','AFG',208),(62,'Djibouti','DJ','AFG',262),(63,'Dominica','DM','AFG',212),(64,'Dominican Republic (the)','DO','AFG',214),(65,'Ecuador','EC','AFG',218),(66,'Egypt','EG','AFG',818),(67,'El Salvador','SV','AFG',222),(68,'Equatorial Guinea','GQ','AFG',226),(69,'Eritrea','ER','AFG',232),(70,'Estonia','EE','AFG',233),(71,'Ethiopia','ET','AFG',231),(72,'Falkland Islands (the) [Malvinas]','FK','AFG',238),(73,'Faroe Islands (the)','FO','AFG',234),(74,'Fiji','FJ','AFG',242),(75,'Finland','FI','AFG',246),(76,'France','FR','AFG',250),(77,'French Guiana','GF','AFG',254),(78,'French Polynesia','PF','AFG',258),(79,'French Southern Territories (the)','TF','AFG',260),(80,'Gabon','GA','AFG',266),(81,'Gambia (the)','GM','AFG',270),(82,'Georgia','GE','AFG',268),(83,'Germany','DE','AFG',276),(84,'Ghana','GH','AFG',288),(85,'Gibraltar','GI','AFG',292),(86,'Greece','GR','AFG',300),(87,'Greenland','GL','AFG',304),(88,'Grenada','GD','AFG',308),(89,'Guadeloupe','GP','AFG',312),(90,'Guam','GU','AFG',316),(91,'Guatemala','GT','AFG',320),(92,'Guernsey','GG','AFG',831),(93,'Guinea','GN','AFG',324),(94,'Guinea-Bissau','GW','AFG',624),(95,'Guyana','GY','AFG',328),(96,'Haiti','HT','AFG',332),(97,'Heard Island and McDonald Islands','HM','AFG',334),(98,'Holy See (the)','VA','AFG',336),(99,'Honduras','HN','AFG',340),(100,'Hong Kong','HK','AFG',344),(101,'Hungary','HU','AFG',348),(102,'Iceland','IS','AFG',352),(103,'India','IN','AFG',356),(104,'Indonesia','ID','AFG',360),(105,'Iran (Islamic Republic of)','IR','AFG',364),(106,'Iraq','IQ','AFG',368),(107,'Ireland','IE','AFG',372),(108,'Isle of Man','IM','AFG',833),(109,'Israel','IL','AFG',376),(110,'Italy','IT','AFG',380),(111,'Jamaica','JM','AFG',388),(112,'Japan','JP','AFG',392),(113,'Jersey','JE','AFG',832),(114,'Jordan','JO','AFG',400),(115,'Kazakhstan','KZ','AFG',398),(116,'Kenya','KE','AFG',404),(117,'Kiribati','KI','AFG',296),(118,'Korea (the Democratic People\'s Republic of)','KP','AFG',408),(119,'Korea (the Republic of)','KR','AFG',410),(120,'Kuwait','KW','AFG',414),(121,'Kyrgyzstan','KG','AFG',417),(122,'Lao People\'s Democratic Republic (the)','LA','AFG',418),(123,'Latvia','LV','AFG',428),(124,'Lebanon','LB','AFG',422),(125,'Lesotho','LS','AFG',426),(126,'Liberia','LR','AFG',430),(127,'Libya','LY','AFG',434),(128,'Liechtenstein','LI','AFG',438),(129,'Lithuania','LT','AFG',440),(130,'Luxembourg','LU','AFG',442),(131,'Macao','MO','AFG',446),(132,'Macedonia (the former Yugoslav Republic of)','MK','AFG',807),(133,'Madagascar','MG','AFG',450),(134,'Malawi','MW','AFG',454),(135,'Malaysia','MY','AFG',458),(136,'Maldives','MV','AFG',462),(137,'Mali','ML','AFG',466),(138,'Malta','MT','AFG',470),(139,'Marshall Islands (the)','MH','AFG',584),(140,'Martinique','MQ','AFG',474),(141,'Mauritania','MR','AFG',478),(142,'Mauritius','MU','AFG',480),(143,'Mayotte','YT','AFG',175),(144,'Mexico','MX','AFG',484),(145,'Micronesia (Federated States of)','FM','AFG',583),(146,'Moldova (the Republic of)','MD','AFG',498),(147,'Monaco','MC','AFG',492),(148,'Mongolia','MN','AFG',496),(149,'Montenegro','ME','AFG',499),(150,'Montserrat','MS','AFG',500),(151,'Morocco','MA','AFG',504),(152,'Mozambique','MZ','AFG',508),(153,'Myanmar','MM','AFG',104),(154,'Namibia','NA','AFG',516),(155,'Nauru','NR','AFG',520),(156,'Nepal','NP','AFG',524),(157,'Netherlands (the)','NL','AFG',528),(158,'New Caledonia','NC','AFG',540),(159,'New Zealand','NZ','AFG',554),(160,'Nicaragua','NI','AFG',558),(161,'Niger (the)','NE','AFG',562),(162,'Nigeria','NG','AFG',566),(163,'Niue','NU','AFG',570),(164,'Norfolk Island','NF','AFG',574),(165,'Northern Mariana Islands (the)','MP','AFG',580),(166,'Norway','NO','AFG',578),(167,'Oman','OM','AFG',512),(168,'Pakistan','PK','AFG',586),(169,'Palau','PW','AFG',585),(170,'Palestine, State of','PS','AFG',275),(171,'Panama','PA','AFG',591),(172,'Papua New Guinea','PG','AFG',598),(173,'Paraguay','PY','AFG',600),(174,'Peru','PE','AFG',604),(175,'Philippines (the)','PH','AFG',608),(176,'Pitcairn','PN','AFG',612),(177,'Poland','PL','AFG',616),(178,'Portugal','PT','AFG',620),(179,'Puerto Rico','PR','AFG',630),(180,'Qatar','QA','AFG',634),(181,'Réunion','RE','AFG',638),(182,'Romania','RO','AFG',642),(183,'Russian Federation (the)','RU','AFG',643),(184,'Rwanda','RW','AFG',646),(185,'Saint Barthélemy','BL','AFG',652),(186,'Saint Helena, Ascension and Tristan da Cunha','SH','AFG',654),(187,'Saint Kitts and Nevis','KN','AFG',659),(188,'Saint Lucia','LC','AFG',662),(189,'Saint Martin (French part)','MF','AFG',663),(190,'Saint Pierre and Miquelon','PM','AFG',666),(191,'Saint Vincent and the Grenadines','VC','AFG',670),(192,'Samoa','WS','AFG',882),(193,'San Marino','SM','AFG',674),(194,'Sao Tome and Principe','ST','AFG',678),(195,'Saudi Arabia','SA','AFG',682),(196,'Senegal','SN','AFG',686),(197,'Serbia','RS','AFG',688),(198,'Seychelles','SC','AFG',690),(199,'Sierra Leone','SL','AFG',694),(200,'Singapore','SG','AFG',702),(201,'Sint Maarten (Dutch part)','SX','AFG',534),(202,'Slovakia','SK','AFG',703),(203,'Slovenia','SI','AFG',705),(204,'Solomon Islands','SB','AFG',90),(205,'Somalia','SO','AFG',706),(206,'South Africa','ZA','AFG',710),(207,'South Georgia and the South Sandwich Islands','GS','AFG',239),(208,'South Sudan','SS','AFG',728),(209,'Spain','ES','AFG',724),(210,'Sri Lanka','LK','AFG',144),(211,'Sudan (the)','SD','AFG',729),(212,'Suriname','SR','AFG',740),(213,'Svalbard and Jan Mayen','SJ','AFG',744),(214,'Swaziland','SZ','AFG',748),(215,'Sweden','SE','AFG',752),(216,'Switzerland','CH','AFG',756),(217,'Syrian Arab Republic','SY','AFG',760),(218,'Taiwan (Province of China)','TW','AFG',158),(219,'Tajikistan','TJ','AFG',762),(220,'Tanzania, United Republic of','TZ','AFG',834),(221,'Thailand','TH','AFG',764),(222,'Timor-Leste','TL','AFG',626),(223,'Togo','TG','AFG',768),(224,'Tokelau','TK','AFG',772),(225,'Tonga','TO','AFG',776),(226,'Trinidad and Tobago','TT','AFG',780),(227,'Tunisia','TN','AFG',788),(228,'Turkey','TR','AFG',792),(229,'Turkmenistan','TM','AFG',795),(230,'Turks and Caicos Islands (the)','TC','AFG',796),(231,'Tuvalu','TV','AFG',798),(232,'Uganda','UG','AFG',800),(233,'Ukraine','UA','AFG',804),(234,'United Arab Emirates (the)','AE','AFG',784),(235,'United Kingdom of Great Britain and Northern ','GB','AFG',826),(236,'United States Minor Outlying Islands (the)','UM','AFG',581),(237,'United States of America (the)','US','AFG',840),(238,'Uruguay','UY','AFG',858),(239,'Uzbekistan','UZ','AFG',860),(240,'Vanuatu','VU','AFG',548),(241,'Venezuela (Bolivarian Republic of)','VE','AFG',862),(242,'Viet Nam','VN','AFG',704),(243,'Virgin Islands (British)','VG','AFG',92),(244,'Virgin Islands (U.S.)','VI','AFG',850),(245,'Wallis and Futuna','WF','AFG',876),(246,'Western Sahara*','EH','AFG',732),(247,'Yemen','YE','AFG',887),(248,'Zambia','ZM','AFG',894),(249,'Zimbabwe','ZW','AFG',716);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `langs`
--

DROP TABLE IF EXISTS `langs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(40) NOT NULL,
  `default` int(11) NOT NULL DEFAULT '0',
  `flag` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `label_UNIQUE` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `langs`
--

LOCK TABLES `langs` WRITE;
/*!40000 ALTER TABLE `langs` DISABLE KEYS */;
INSERT INTO `langs` VALUES (1,'EN',1,'US'),(2,'ES',0,'MX');
/*!40000 ALTER TABLE `langs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `limits`
--

DROP TABLE IF EXISTS `limits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_id` int(10) unsigned DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `start_on` tinyint(1) unsigned DEFAULT NULL,
  `min_nights` tinyint(3) unsigned DEFAULT NULL,
  `max_nights` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `room_id` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `limits`
--

LOCK TABLES `limits` WRITE;
/*!40000 ALTER TABLE `limits` DISABLE KEYS */;
/*!40000 ALTER TABLE `limits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locale`
--

DROP TABLE IF EXISTS `locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locale` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_iso` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `dir` enum('ltr','rtl') DEFAULT 'ltr',
  `sort` int(10) unsigned DEFAULT NULL,
  `is_default` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `language_iso` (`language_iso`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locale`
--

LOCK TABLES `locale` WRITE;
/*!40000 ALTER TABLE `locale` DISABLE KEYS */;
INSERT INTO `locale` VALUES (1,'en-US','English','us','ltr',1,1),(2,'es-MX','Español','MX','ltr',NULL,0);
/*!40000 ALTER TABLE `locale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multilang`
--

DROP TABLE IF EXISTS `multilang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `multilang` (
  `field` varchar(45) NOT NULL,
  `lang` varchar(45) NOT NULL,
  `value` text NOT NULL,
  `foreign_id` int(11) NOT NULL,
  `model` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multilang`
--

LOCK TABLES `multilang` WRITE;
/*!40000 ALTER TABLE `multilang` DISABLE KEYS */;
INSERT INTO `multilang` VALUES ('room-description','1','Category Apartment',1,'rooms'),('room-description','2','Cátegoria Departmento.',1,'rooms'),('room-name','1','Apartment',1,'rooms'),('room-name','2','Departmamento',1,'rooms'),('room-description','1','Double Bed Room Description',2,'rooms'),('room-description','2','Descripción de la habitación cama doble.',2,'rooms'),('room-name','1','Double Bed Room',2,'rooms'),('room-name','2','Habitación Doble Cama',2,'rooms');
/*!40000 ALTER TABLE `multilang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `foreign_id` int(10) unsigned NOT NULL,
  `tab_id` int(10) unsigned DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `adults` tinyint(3) unsigned DEFAULT NULL,
  `children` tinyint(3) unsigned DEFAULT NULL,
  `mon` decimal(9,2) unsigned DEFAULT NULL,
  `tue` decimal(9,2) unsigned DEFAULT NULL,
  `wed` decimal(9,2) unsigned DEFAULT NULL,
  `thu` decimal(9,2) unsigned DEFAULT NULL,
  `fri` decimal(9,2) unsigned DEFAULT NULL,
  `sat` decimal(9,2) unsigned DEFAULT NULL,
  `sun` decimal(9,2) unsigned DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  `fake_delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `season` (`foreign_id`,`tab_id`,`season`,`adults`,`children`),
  UNIQUE KEY `dates` (`foreign_id`,`date_from`,`date_to`,`tab_id`,`adults`,`children`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES (1,1,NULL,'Winter','2016-11-01','2016-12-24',NULL,NULL,100.00,100.00,100.00,100.00,999.99,999.99,999.99,0,0),(2,1,NULL,'Default','2016-11-01','2016-12-24',NULL,NULL,299.00,299.00,299.00,299.00,299.00,299.00,299.00,0,0);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_id` int(11) DEFAULT '0',
  `adults` int(11) DEFAULT '0',
  `childrens` int(11) DEFAULT '0',
  `count` int(11) DEFAULT '0',
  `image` tinytext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,0,2,0,20,'0'),(2,0,2,0,20,'0');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(125) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(32) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` date DEFAULT NULL,
  `fake_delete` tinyint(1) DEFAULT '0',
  `logons` int(11) DEFAULT NULL,
  `name` varchar(125) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sadmin',NULL,'evelioamd@outlook.com','662601cbc5941bca8f2709a652eaeb8f982b79f626269ce6b9b8bd57dfccb060dd6fd465c65a02c139ea59129900e5b023244d7516083b3961bc50b5b09facde','w2022vrEqrhVYVezL7aWHKIsx','2016-09-23 15:02:26',NULL,0,NULL,'Evelio Mis',0),(2,'sadmin',NULL,'webmasterjr@original-group.com','662601cbc5941bca8f2709a652eaeb8f982b79f626269ce6b9b8bd57dfccb060dd6fd465c65a02c139ea59129900e5b023244d7516083b3961bc50b5b09facde','w2022vrEqrhVYVezL7aWHKIsx','2016-09-23 15:02:26',NULL,0,NULL,'Jose Luis',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'originalbooking'
--
/*!50003 DROP PROCEDURE IF EXISTS `CLEAN_LANGS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CLEAN_LANGS`(IN _foreign_id INT, IN _field VARCHAR(45), IN _model VARCHAR(45))
BEGIN
	DELETE FROM 
		multilang 
	WHERE
		`foreign_id` = _foreign_id
	AND
        `field` = _field
	AND
        `model` = _model
	;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `saveMultilang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `saveMultilang`(IN _field VARCHAR(45), IN _lang INT, IN _value TEXT, IN _foreign_id INT, IN _model VARCHAR(45))
BEGIN
	DELETE FROM multilang WHERE lang = _lang AND foreign_id = _foreign_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `saveRoom` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `saveRoom`(IN _ID INT, IN _property_id INT, IN _adults INT, IN _childrens INT, IN _count INT, IN _image INT)
BEGIN
	IF _ID > 0 THEN
		/*UPDATE*/
		UPDATE 
			`booking`.`rooms`
		SET
			`property_id` = _property_id,
			`adults` = _adults,
			`childrens` = _childrens,
			`count` = _count,
			`image` = _image
		WHERE
			id =  _ID
		;
		SET @ID_ROOM = _ID;
	ELSE
		/*INSERT*/
		INSERT INTO 
			`booking`.`rooms`
			(
				`property_id`,
				`adults`,
				`childrens`,
				`count`,
                `image`
			)
		VALUES
			(
				_property_id,
				_adults,
				_childrens,
				_count,
				_image
			)
		;
		SET @ID_ROOM = LAST_INSERT_ID();
    END IF;
	
    
    IF NULLIF(@ID_ROOM, '') IS NOT NULL THEN
		SELECT @ID_ROOM as ID_ROOM;
    ELSE
		SELECT NULL AS ID_ROOM;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SAVE_MULTILANG` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SAVE_MULTILANG`(IN _field VARCHAR(45), IN _lang VARCHAR(45), IN _value TEXT, IN _foreign_id INT, IN _model VARCHAR(45))
BEGIN
	INSERT INTO 
		multilang(`field`,`lang`,`value`,`foreign_id`,`model`)
	VALUES
		(_field, _lang,_value, _foreign_id, _model)
	;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SAVE_PRICE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SAVE_PRICE`(
	IN _id INT, 
    IN _foreign_id INT, 
    IN _season VARCHAR(255), 
    IN _date_from DATE, 
    IN _date_to DATE, 
    IN _mon DECIMAL(9,2),
    IN _tue DECIMAL(9,2),
    IN _wed DECIMAL(9,2),
    IN _thu DECIMAL(9,2),
    IN _fri DECIMAL(9,2),
    IN _sat  DECIMAL(9,2),
    IN _sun DECIMAL(9,2)
)
BEGIN
	IF _id > 0 THEN
		/*UPDATE*/
		UPDATE 
			`prices`
		SET
			`foreign_id` = _foreign_id,
			`season` = _season,
			`date_from` = _date_from,
			`date_to` = _date_to,
			`mon` = _mon,
			`tue` = _tue,
			`wed` = _wed,
			`thu` = _thu,
			`fri` = _fri,
			`sat` = _sat,
			`sun` = _sun
		WHERE
			id =  _id
		;
		SET @ID_ROOM = _id;
	ELSE
		/*INSERT*/
		INSERT INTO 
			`prices`
			(
				`foreign_id`,
				`season`,
				`date_from`,
				`date_to`,
				`mon`,
				`tue`,
				`wed`,
				`thu`,
				`fri`,
				`sat`,
				`sun`
			)
		VALUES
			(
				_foreign_id,
				_season,
				_date_from,
				_date_to,
				_mon,
				_tue,
				_wed,
				_thu,
				_fri,
				_sat,
				_sun
			)
		;
		SET @ID_PRICE = LAST_INSERT_ID();
    END IF;
	
    
    IF NULLIF(@ID_PRICE, '') IS NOT NULL THEN
		SELECT @ID_PRICE as ID_PRICE;
    ELSE
		SELECT NULL AS ID_PRICE;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SAVE_ROOM` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SAVE_ROOM`(IN _ID INT, IN _property_id INT, IN _adults INT, IN _childrens INT, IN _count INT, IN _image INT)
BEGIN
	IF _ID > 0 THEN
		/*UPDATE*/
		UPDATE 
			`booking`.`rooms`
		SET
			`property_id` = _property_id,
			`adults` = _adults,
			`childrens` = _childrens,
			`count` = _count,
			`image` = _image
		WHERE
			id =  _ID
		;
		SET @ID_ROOM = _ID;
	ELSE
		/*INSERT*/
		INSERT INTO 
			`booking`.`rooms`
			(
				`property_id`,
				`adults`,
				`childrens`,
				`count`,
                `image`
			)
		VALUES
			(
				_property_id,
				_adults,
				_childrens,
				_count,
				_image
			)
		;
		SET @ID_ROOM = LAST_INSERT_ID();
    END IF;
	
    
    IF NULLIF(@ID_ROOM, '') IS NOT NULL THEN
		SELECT @ID_ROOM as ID_ROOM;
    ELSE
		SELECT NULL AS ID_ROOM;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `showRoom` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `showRoom`(IN _lang INT)
BEGIN
	SELECT 
		* 
	FROM
		rooms AS tr
	LEFT JOIN
		multilang AS tl
	ON
		tr.ID = tl.foreign_id
	AND 
		tl.model = 'rooms'
	;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `ViewRooms`
--

/*!50001 DROP VIEW IF EXISTS `ViewRooms`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ViewRooms` AS select `tr`.`id` AS `id`,`tr`.`adults` AS `adults`,`tr`.`childrens` AS `childrens`,`tm`.`value` AS `category` from (`rooms` `tr` join `multilang` `tm` on(((`tm`.`model` = 'rooms') and (`tm`.`field` = 'room-name') and (`tm`.`foreign_id` = `tr`.`id`) and (`tm`.`lang` = (select `langs`.`id` from `langs` where (`langs`.`default` = 1) limit 1))))) order by `tr`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ViewRoomsDetails`
--

/*!50001 DROP VIEW IF EXISTS `ViewRoomsDetails`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ViewRoomsDetails` AS select `multilang`.`field` AS `field`,`multilang`.`lang` AS `lang`,`multilang`.`value` AS `value`,`multilang`.`foreign_id` AS `foreign_id`,`multilang`.`model` AS `model` from `multilang` where (`multilang`.`model` = 'rooms') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-31 16:14:52
