<?php
namespace App\Models;

class Model {
	protected $conn;

	public function __construct($conn) {
		$this->conn = $conn;
	}

	public function getError() {
		return $this->error;
	}

	public function setError($error = []) {
		$this->error = $error;
	}

	public function getErrorCode() {
		return isset($this->error['code']) ? $this->error['code'] : 0;
	}

	public function getErrorInfo() {
		return isset($this->error['info']) ? $this->error['info'] : 0;
	}
}