<?php
namespace App\Models;

use \App\Models\Model;

class LangsModel extends Model {

	public function getLangs() {

		return $this->conn->fetchAll("SELECT * FROM locale;");

	}

	public function getLang($id = 0) {
		return $this->conn->fetchAssoc("SELECT * FROM locale WHERE id = ?;", [$id]);
	}

	public function saveLang($id, $data, $is_default = 0) {
		//change the current value of the default language.
		if ($is_default == 1) {
			$this->conn->update(
				'locale',
				[
					'is_default' => 0,
				],
				['is_default' => 1]
			);
		}
		if (0 < $id) {
			//update
			$this->conn->update(
				'locale',
				[
					'name' => $data['lang-label'],
					'language_iso' => $data['lang-iso'],
					'flag' => $data['lang-flag'],
					'is_default' => ($is_default == 1 ? 1 : 0),
				],
				[
					'id' => $id,
				]
			);

		} else {
			//insert
			$this->conn->insert(
				'locale',
				[
					'name' => $data['lang-label'],
					'flag' => $data['lang-flag'],
					'language_iso' => $data['lang-iso'],
					'is_default' => ($is_default == 1 ? 1 : 0),
				]
			);

		}

		if ($this->conn->errorCode() != '00000') {
			$this->setError([
				'code' => $this->conn->errorCode(),
				'context' => 'LangsModel',
				'info' => _('An error occurs while trying to save the info'),
			]);
			return false;
		}

		//we return the same id if an update or new id if is an insert.
		return $id > 0 ? $id : $this->conn->lastInsertId();
	}
}