<?php
namespace App\Models;

use App\Models\Model;

class UserModel extends Model {
	static $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private $id;
	private $role_id;
	private $email;
	private $password;
	private $name;
	private $phone;
	private $created;
	private $last_login;
	private $status;
	private $is_active;
	private $ip;
	private $salt;

	private function updateUserInfo($data) {
		$this->id = $data['id'];
		$this->role_id = $data['role_id'];
		$this->email = $data['email'];
		$this->password = $data['password'];
		$this->name = $data['name'];
		$this->phone = $data['phone'];
		$this->created = $data['created'];
		$this->last_login = $data['last_login'];
		$this->status = $data['status'];
		$this->is_active = $data['is_active'];
		$this->ip = $data['ip'];
		$this->salt = $data['salt'];
	}

	public function setUserId($id = 0) {
		if ($id > 0 && $this->id !== $id) {
			$row = $this->conn->fetchAssoc('SELECT * FROM users WHERE email = ?', [$id]);
			if ($row) {
				$this->updateUserInfo($row);
				//the user was found
				return true;
			}
			//the user was not found
			return false;
		}
	}

	public function setUserEmail($email) {
		if (!empty($email)) {
			$row = $this->conn->fetchAssoc('SELECT * FROM users WHERE email = ?', [$email]);
			if ($row) {
				$this->updateUserInfo($row);
				//the user was found
				return true;
			}
			//the user was not found
			return false;
		}
	}

	public function getUserId() {
		return $this->id;
	}

	private function getBase62Char($num) {
		return self::$chars[$num];
	}

	public function generateSalt($saltLength = 25) {
		$randString = "";

		for ($i = 0; $i < $saltLength; $i++) {
			$randChar = self::getBase62Char(mt_rand(0, 61));
			$randString .= $randChar;
		}

		return $randString;
	}

	public function generateHash($password, $salt) {
		$hash = $password . $salt;
		for ($i = 0; $i < 50; $i++) {
			$hash = hash('sha512', $password . $hash . $salt);
		}
		return $hash;
	}

	public function verifyHash($password) {
		$hash = $this->generateHash($password, $this->salt);
		return ($hash === $this->password);
	}

	function getVisibleData() {
		return [
			'id' => $this->id,
			'role_id' => $this->role_id,
			'email' => $this->email,
			'name' => $this->name,
			'phone' => $this->phone,
		];
	}
}