<?php
namespace App\Models;

use App\Models\Model;

class RoomsModel extends Model {

	public function saveRoom($id, $room = []) {
		//save the room first.
		if ($id > 0) {
			//update
			$this->conn->update(
				'rooms', [
					'adults' => $room['adults'],
					'childrens' => $room['childrens'],
					'count' => $room['count'],
				], [
					'id' => $id,
				]
			);
		} else {
			//insert
			$this->conn->insert(
				'rooms', [
					'adults' => $room['adults'],
					'childrens' => $room['childrens'],
					'count' => $room['count'],
				]
			);
			//overwrite the id.
			$id = $this->conn->lastInsertId();
		}

		//save the multilang
		$this->saveMultilang($id, $room['multilang']);

		return $id;

	}

	private function saveMultilang($roomId, $multilang = []) {

		foreach ($multilang as $lang_index => $fields) {
			//TODO continuar con el guardado del multilenguaje.
			//TODO tambien generar el room count, para el inventario.
			foreach ($fields as $k => $v) {
				//first we clean all previous match of the index.
				$this->conn->delete('multilang', [
					'foreign_id' => $roomId,
					'model' => 'rooms',
					'lang' => $lang_index,
					'field' => $k,
				]);

				//then we insert the new values, even if nothing change.
				$this->conn->insert('multilang', [
					'foreign_id' => $roomId,
					'model' => 'rooms',
					'lang' => $lang_index,
					'field' => $k,
					'value' => $v,
				]);

			}
		}

	}

	public function getRoom($id) {

		//get first the data of the room
		$roomData = $this->conn->fetchAssoc('SELECT * FROM rooms WHERE id = ?;', [$id]);

		if (!$roomData) {
			return false;
		}

		$langs = $this->conn->fetchAll('SELECT id FROM locale;');

		foreach ($langs as $lang) {
			$tmpRs = $this->conn->fetchAll('SELECT * FROM multilang WHERE foreign_id = ? AND lang = ?;', [$id, $lang['id']]);

			foreach ($tmpRs as $rs) {
				$roomData['multilang'][$lang['id']][$rs['field']] = $rs['value'];
			}
		}

		return $roomData;
	}

	public function getRooms() {
		return $this->conn->fetchAll("SELECT *, (SELECT value FROM multilang WHERE foreign_id = rooms.id AND field = 'room-category' AND model = 'rooms' AND lang = (SELECT locale.id from locale WHERE locale.is_default = 1)) category FROM rooms;");
	}

}