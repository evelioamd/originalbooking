<?php
namespace App\Routes;

use App\Controllers\FrontControllers;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class FrontRoutes implements ControllerProviderInterface {
	public function connect(Application $app) {

		$app['front.controllers'] = function ($app) {
			return new FrontControllers($app);
		};

		$front = $app['controllers_factory'];

		//$front->before('login.controllers:isUserLogged');

		$front
			->get(
				'/',
				'front.controllers:index'
			)
			->bind('front.index')
		;

		return $front;
	}
}