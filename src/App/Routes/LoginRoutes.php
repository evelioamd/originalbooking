<?php
namespace App\Routes;

use App\Controllers\LoginControllers;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class LoginRoutes implements ControllerProviderInterface {
	public function connect(Application $app) {

		$app['login.controllers'] = function ($app) {
			return new LoginControllers($app);
		};

		$root = $app['controllers_factory'];

		$root->get('/login/', 'login.controllers:index')
			->bind('login')
		;

		/*Login Routes*/
		$root->post(
			'/login_check',
			'login.controllers:AdminLogin'
		)
			->bind('login_check')
		;

		$root->get(
			'/logout/',
			'login.controllers:AdminLogout'
		)
			->bind('logout');

		return $root;

	}
}
