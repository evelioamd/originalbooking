<?php
namespace App\Routes;

use App\Controllers\RoomsControllers;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class RoomsRoutes implements ControllerProviderInterface {
	public function connect(Application $app) {

		$app['rooms.controllers'] = function ($app) {
			return new RoomsControllers($app);
		};

		$rooms = $app['controllers_factory'];

		$rooms->before('login.controllers:isUserLogged');

		$rooms
			->get('/', 'rooms.controllers:index')
			->bind('rooms.index')
		;

		$rooms
			->get(
				'/{id}/',
				'rooms.controllers:show'
			)
			->bind('rooms.show')
			->assert('id', '\d+')
		;

		$rooms
			->post(
				'/{id}/',
				'rooms.controllers:store'
			)
			->bind('rooms.store')
			->assert('id', '\d+')
		;

		return $rooms;
	}
}