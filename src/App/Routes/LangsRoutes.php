<?php
namespace App\Routes;

use App\Controllers\LangsControllers;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

class LangsRoutes implements ControllerProviderInterface {
	public function connect(Application $app) {

		$app['langs.controllers'] = function ($app) {
			return new LangsControllers($app);
		};

		$langs = $app['controllers_factory'];

		$langs->before('login.controllers:isUserLogged');

		$langs
			->get(
				'/',
				'langs.controllers:index'
			)
			->bind('langs.index')
		;

		$langs
			->get(
				'/{id}/',
				'langs.controllers:show'
			)
			->method('GET')
			->bind('langs.showLang')
		;

		$langs
			->post(
				'/{id}/',
				'langs.controllers:store'
			)
			->bind('langs.saveLang')
		;

		$langs
			->match(
				'/{id}/delete',
				'langs.controllers:delete'
			)
			->method('GET|POST')
			->bind('langs.deleteLang')
		;

		return $langs;
	}
}