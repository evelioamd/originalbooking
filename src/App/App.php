<?php
namespace App;

use \Silex\Application;

class App {

	protected $app;

	function __construct(Application $app) {
		$this->app = $app;
	}

	//explicity for controllers, clear the session values
	/**
	 * This function should be called by the methods that read session vars.
	 * @return void
	 */
	function clearSession() {

		$this->app['session']->set('alerts', false);
		$this->app['session']->set('form', false);
		$this->app['session']->set('validations', false);

	}

	public function t($key) {
		return $this->app['translator']->trans($key);
	}

}