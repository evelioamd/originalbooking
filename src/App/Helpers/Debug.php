<?php
namespace App\Helpers;

Class Debug {

	function __construct() {
	}
	public static function _write($info = '') {

		$log = print_r(array(
			'hour' => \date("Y-n-j H:m:s"),
			'url' => $_SERVER['REQUEST_URI'],
			//'trace' => \parse_trace(\debug_backtrace()),
			'info' => $info,
		),
			true
		) . "-------------------------" . PHP_EOL;
		file_put_contents('./log_' . date("Y-n-j") . '.log', $log, FILE_APPEND);

	}

	public static function _print($info = '') {
		echo '<pre>';
		print_r(array(
			'hour' => \date("Y-n-j H:m:s"),
			'url' => $_SERVER['REQUEST_URI'],
			//'trace' => debug_backtrace(),
			'info' => $info,
		)
		);
		echo "-------------------------" . PHP_EOL;
		echo '</pre>';
	}
}