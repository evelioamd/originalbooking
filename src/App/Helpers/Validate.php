<?php
namespace App\Helpers;

use Symfony\Component\Validator\Constraints as Assert;

class Validate {

	private $Validator;

	function __construct($Validator) {
		$this->Validator = $Validator;
	}

	function NotBlank($value) {
		return $this->Validator->validate($value, new Assert\NotBlank());
	}

	function Email($value) {
		return $this->Validator->validate($value, new Assert\Email());
	}

	function Length($value, $min = 40, $max = 1024) {
		return $this->Validator->validate($value, new Assert\Length(['min' => $min, 'max' => $max]));
	}

	function GreaterThan($value, $min = 0) {
		return $this->Validator->validate($value, new Assert\GreaterThan($min));
	}
}