<?php
namespace App\Controllers;

use \App\App;

class LoginControllers extends App {

	public function index() {
		\App\Helpers\Debug::_write($this->app['translator']->trans('updated'));
		return $this->app['twig']->render('login/login.html.twig');
	}

	public function adminLogin() {

		$request = $this->app['request_stack']->getCurrentRequest();

		//get the user data
		$inputEmail = $request->request->get('email');
		$inputPass = $request->request->get('password');

		//redirect there if has some value
		$forward = $request->request->get('forward');

		//at least the email field is set?.
		if ($inputEmail) {

			$user = $this->app['UsersModel'];
			$user->setUserEmail($inputEmail);

			$loginValid = $user->verifyHash($inputPass);

			\App\Helpers\Debug::_write($loginValid);

			if ($loginValid === true) {
				$this->app['session']->set('userlogged', $user->getVisibleData());

			} else {
				$this->app['session']->getFlashBag()->clear('loginerror'); //clear prvious request errors
				$this->app['session']->getFlashBag()->add('loginerror', 'Password or user not found');
			}

			//check where to redirect
			if ($forward) {
				$redirect = $forward;
				//Debug::_print('aqui');
			} else {
				$redirect = $this->app['url_generator']->generate('front.index');

			}

		}

		return $this->app->redirect($redirect);
	}

	public function adminLogout() {

		$this->app['session']->remove('userlogged');

		if (ini_get("session.use_cookies")) {
			$params = session_get_cookie_params();
			setcookie(session_name(), '', time() - 42000,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
			);
		}

		$this->app['session']->invalidate(1);

		return $this->app->redirect($this->app['url_generator']->generate('front.index'));

	}

	public function isUserLogged() {
		$user = $this->app['session']->get('userlogged');
		$request = $this->app['request_stack']->getCurrentRequest();
		$uri = $request->getRequestUri();

		if (null === $user) {
			return $this->app->redirect($this->app['url_generator']->generate('front.index', ['redirect' => $uri]));
		}

		$this->app['request_stack']->getCurrentRequest();
	}
}