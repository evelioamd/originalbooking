<?php
namespace App\Controllers;

use \App\App;
use \Silex\Application;

/**
 * @mix $app Instance of the Silex Application.
 */
class FrontControllers extends App {

	/**
	 * Show index page
	 * @return \Silex\Response
	 */
	public function index() {
		//$db = $this->app['db'];
		$user = $this->app['session']->get('userlogged');

		if (null === $user) {
			$request = $this->app['request_stack']->getCurrentRequest();
			$uri = $request->getRequestUri();
			return $this->app->redirect($this->app['url_generator']->generate('login', ['redirect' => $uri]));
		}

		return $this->app['twig']->render('index.html.twig', array());
	}

} //end of class