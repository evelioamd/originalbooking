<?php
/**
 * Manage ll routes involving with the languages.
 *
 */

namespace App\Controllers;

use \App\App;
use \App\Helpers\Validate;
use \Silex\Application;

/**
 * @mix $app Instance of the Silex Application.
 */
class LangsControllers extends App {

	private $viewData;

	public function index() {
		$db = $this->app['db'];

		$this->viewData['title'] = $this->t('Languages');

		$this->viewData['langs'] = false;
		//fetch the list of langs

		$this->viewData['langs'] = $this->app['LangsModel']->getLangs();

		$this->clearSession();
		return $this->app['twig']->render('langs/index.html.twig', $this->viewData);
	}

	public function show($id) {

		$this->viewData['title'] = ($id > 0 ? 'Update ' : 'Add ') . ' language';
		$this->viewData['id'] = $id;

		$this->viewData['form'] = $this->app['session']->get('form');

		if (0 < $id && empty($this->viewData['form'])) {
			//fetch the current register form db.
			$LangModel = $this->app['LangsModel'];
			$lang = $LangModel->getLang($id);
			$this->viewData['form'] = [
				'id' => $lang['id'],
				'lang-label' => $lang['name'],
				'lang-default' => $lang['is_default'],
				'lang-flag' => $lang['flag'],
				'lang-iso' => $lang['language_iso'],
			];
		}

		//\App\Helpers\Debug::_write($this->viewData);
		$validations = $this->app['session']->get('validations');
		$this->viewData['validations'] = false;
		if ($validations) {
			$this->viewData['validations'] = $validations;
		}

		$alerts = $this->app['session']->get('alerts');
		$this->viewData['alerts'] = false;
		if ($alerts) {
			$this->viewData['alerts'] = $alerts;
		}

		$this->clearSession();
		return $this->app['twig']->render('langs/add.html.twig', $this->viewData);
	}

	/**
	 * Save record to the database
	 *
	 * It can perform both insert and update based on the method and the {id} parameter of the request.
	 *
	 * @return \Silex\Request;
	 */
	public function store($id) {

		$request = $this->app['request_stack']->getCurrentRequest();

		//get the form data
		$form = $request->request->all();

		$Validate = new Validate($this->app['validator']);

		//validate the fields
		$errors['lang-label'] = $Validate->NotBlank($form['lang-label']);
		$errors['lang-flag'] = $Validate->NotBlank($form['lang-flag']);
		$errors['lang-iso'] = $Validate->NotBlank($form['lang-iso']);

		$haveError = false;
		foreach ($errors as $k => $error) {
			if (count($error) > 0) {
				$haveError = true;
				$errors[$k] = (string) $error;
			}
		}

		if ($haveError === true) {

			$this->app['session']->set('validations', $errors);
			$this->app['session']->set('form', $form);
			$this->app['session']->set('alerts', ['type' => 'error', 'message' => 'Error while validating the info, check details on the form.']);
			return $this->app->redirect($this->app['url_generator']->generate('langs.showLang', ['id' => $id]));
		}

		//\App\Helpers\Debug::_write($form);
		$LangModel = $this->app['LangsModel'];
		$result = $LangModel->saveLang($id, $form, $form['lang-default']);

		//saving or updating fails
		if ($result === false) {
			//log the error
			\App\Helpers\Debug::_write($LangModel->getError());

			//set front alert
			$alert = [
				'type' => 'error',
				'message' => $LangModel->getInfo(),
			];
		} else {
			//set front alert
			$alert = [
				'type' => 'success',
				'message' => sprintf(_('Language was %s successfully.'), ($id > 0 ? _('updated') : _('added'))),
			];

			//new id or overwrite in case of insert.
			$id = $result;
		}

		$redirect = $this->app['url_generator']->generate('langs.showLang', ['id' => $id]);
		$this->app['session']->set('alerts', $alert);

		return $this->app->redirect($redirect);
	}

	/**
	 * Delete a lang from the available langs*
	 *
	 * This route is invoked in two steps:
	 * Step 1 shows a confirm dialog, and set an unique value on the sesssion and the form request to step 2.
	 * Step 2 validate the unique value agaisnt the one stored in the session.
	 *
	 *
	 * @return @Silex\Response Redirect or show for to confirm delete.
	 */
	function delete() {

		$request = $this->app['request_stack']->getCurrentRequest();

		//first step
		$this->viewData['title'] = 'Confirm';
		$id = $request->attributes->get('id');
		$this->viewData['id'] = $id;

		$step = $request->query->get('step');

		if ((int) $step == 2) {
			//check the csfr value and delete the row.
			$csfr = $request->request->get('csfr');
			$session_csfr = $this->app['session']->get('csfr');

			if ($csfr === $session_csfr) {
				$db = $this->app['db'];
				$db->delete('langs', ['id' => $id]);
				$this->app['session']->set('alerts', ['type' => 'success', 'message' => 'Language deleted successfully.']);
				$redirect = $this->app['url_generator']->generate('langs.index');
			} else {
				$this->app['session']->set('alerts', ['type' => 'error', 'message' => 'Couldn\'t validate delete code.']);
				$redirect = $this->app['url_generator']->generate('langs.deleteLang', ['id' => $id, 'step' => 1]);
			}

			return $this->app->redirect($redirect);

		} else {

			$defaultLangId = $this->app['db']->fetchAssoc('SELECT id FROM locale WHERE `is_default` = 1;');

			$this->viewData['deletable'] = true;
			if ($id == $defaultLangId['id']) {
				$this->viewData['deletable'] = false;
			} else {
				$this->viewData['deletable'] = true;
				$this->viewData['csfr'] = uniqid();
				$this->app['session']->set('csfr', $this->viewData['csfr']);
			}

			//print the form
			$this->viewData['alerts'] = $this->app['session']->get('alerts');
			return $this->app['twig']->render('langs/delete.html.twig', $this->viewData);
		}

	}

} //end of class