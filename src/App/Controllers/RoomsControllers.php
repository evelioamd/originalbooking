<?php
namespace App\Controllers;

use App\App;
use App\Helpers\Validate;
use Symfony\Component\HttpFoundation\Request;

class RoomsControllers extends App {

	/**
	 * Show list of the rooms available, actives and inactives.
	 * @return Response http Standard Response.
	 */
	public function index() {
		$request = $this->app['request_stack']->getCurrentRequest();

		$db = $this->app['db'];

		$viewData['title'] = 'Rooms';

		$viewData['rooms'] = false;
		//fetch the list of langs

		$roomsModel = $this->app['RoomsModel'];

		$viewData['rooms'] = $roomsModel->getRooms();

		//clear session vars for next request.
		$this->clearSession();

		//ajax or normal petition.
		if ($request->isXmlHttpRequest()) {
			return $this->app->json($viewData['rooms']);
		}

		return $this->app['twig']->render('rooms/index.html.twig', $viewData);
	}

	//show rooms
	public function show($id) {

		$db = $this->app['db'];
		$viewData['id'] = $id;
		//default languages
		$viewData['availableLangs'] = $db->fetchAll('SELECT * FROM locale ORDER BY `is_default` DESC;');

		$validations = $this->app['session']->get('validations');
		$viewData['validations'] = false;
		if ($validations) {
			$viewData['validations'] = $validations;
		}

		$alerts = $this->app['session']->get('alerts');
		$viewData['alerts'] = false;
		if ($alerts) {
			$viewData['alerts'] = $alerts;
		}

		$viewData['form'] = $this->app['session']->get('form');

		if (0 < $id && empty($viewData['form'])) {
			//fetch the current register form db.

			$roomModel = $this->app['RoomsModel'];
			$viewData['form'] = $roomModel->getRoom($id);
		}

		//$row = $db->fetchAssoc('SELECT * FROM rooms WHERE ID = ?;', [(int) $id]);

		if ($id > 0 && !$viewData['form']) {
			$this->app['session']->set('alerts', ['type' => 'error', 'message' => 'Room not found']);
			return $this->app->redirect($this->app['url_generator']->generate('rooms.show', ['id' => 0]));
		}

		$viewData['title'] = $id > 0 ? 'Update ' : 'Add ' . 'Room';
		\App\Helpers\Debug::_write($viewData);
		//clear session vars for next request.
		$this->clearSession();

		return $this->app['twig']->render('rooms/add.html.twig', $viewData);

	}

	//save rooms
	public function store($id) {

		$request = $this->app['request_stack']->getCurrentRequest();
		$db = $this->app['db'];

		$Validate = new Validate($this->app['validator']);

		$form = $request->request->all();

		//validate the form
		//validations of the descriptions and names.

		$errors['adults'] = $Validate->GreaterThan($form['adults'], 0);
		$errors['count'] = $Validate->GreaterThan($form['count'], 0);
		//$errors['price'] = $Validate->GreaterThan($form['price'], 0);

		$haveError = false;
		foreach ($errors as $k => $error) {
			if (count($error) > 0) {
				$haveError = true;
				$errors[$k] = (string) $error;
			} else {
				$errors[$k] = false;
			}
		}

		//validate after the plain values.
		foreach ($form['multilang'] as $lang_index => $fields) {

			foreach ($fields as $k => $v) {
				$tmpError = $Validate->NotBlank($v, 20);

				if (count($tmpError) > 0) {
					$errors['multilang'][$lang_index][$k] = (string) $tmpError;
				} else {
					$errors['multilang'][$lang_index][$k] = false;
				}
				$tmpError = false;
			}

		} //end multilang validation fields.

		if ($haveError === true) {
			$this->app['session']->set('validations', $errors);
			$this->app['session']->set('form', $form);
			return $this->app->redirect($this->app['url_generator']->generate('rooms.show', ['id' => 0]));
		}

		//update or insert
		$roomModel = $this->app['RoomsModel'];
		$id = $roomModel->saveRoom($id, $form);

		$redirect = $this->app['url_generator']->generate('rooms.show', ['id' => $id]);
		return $this->app->redirect($redirect);

	}

	/**
	 * Fetch room name
	 * @return [type] [description]
	 */
	public function getRoomName($room) {

		$db = $this->app['db'];
		\App\Helpers\Debug::_write($room);
		if ($room_name = $db->fetchAssoc('SELECT category FROM ViewRooms WHERE id = ?;', [$room])) {
			return $room_name['category'];
		} else {
			$this->app['session']->set('alerts', ['type' => 'error', 'message' => 'Room not found, it can\'t be added a price']);
			return $this->app->redirect($this->app['url_generator']->generate('prices.index'));
		}
	}

}