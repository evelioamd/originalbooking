<?php
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Loader\PoFileLoader;

session_start();

$app = new Application();

$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new SessionServiceProvider()); //sessiones
$app->register(new ValidatorServiceProvider());
$app['twig'] = $app->extend('twig', function ($twig, $app) {
	// add custom globals, filters, tags, ...
	return $twig;
});

if (getenv('APP_DEV') == 'development') {
	$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
		'db.options' => array(
			'driver' => 'pdo_mysql',
			'host' => 'localhost',
			'dbname' => 'originalbooking',
			'user' => 'root',
			'password' => 'root',
			'charset' => 'utf8mb4',

		),
	));
} else {
	$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
		'db.options' => array(
			'driver' => 'pdo_mysql',
			'host' => 'localhost',
			'dbname' => 'orgpdev_bengine',
			'user' => 'orgpdev_bengine',
			'password' => '^CDMxtcZ*b40',
			'charset' => 'utf8mb4',

		),
	));
}

$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
	'locale_fallbacks' => array('en_US'),
));

$app->extend('translator', function ($translator, $app) {
	$translator->addLoader('po', new PoFileLoader());
	$translator->addResource('po', __DIR__ . '/../locale/es_MX/LC_MESSAGES/es_MX.po', 'es_MX');

	return $translator;
});

$app->before(function (Request $request, Application $app) {

	//is there cookie set?
	$currentLang = isset($_COOKIE['currentLocale']) ? $_COOKIE['currentLocale'] : 'en_US';

	//new lang
	if (isset($_GET['setLocale'])) {
		$currentLang = $_GET['setLocale'];
	}

	//save the language cookie
	$app['translator']->setLocale($currentLang);

	setcookie('currentLocale', $currentLang, strtotime('+90 days'), '/');
	return $app;
	//\App\Helpers\Debug::_write(I18N::getAutoDetectedLanguage());

});

$app->get(
	'/test',
	function () use ($app) {
		//echo $app['translator']->getLocale();
		//\App\Helpers\Debug::_print($app['translator'], true);
		//$app['translator']->setLocale('es_MX');
		//echo $app['translator']->getLocale();
		return $app['translator']->trans('updated');
	}
);

return $app;
