<?php

use App\Routes\FrontRoutes;
use App\Routes\LangsRoutes;
use App\Routes\LoginRoutes;
use App\Routes\RoomsRoutes;

$app->mount('/', new FrontRoutes());

/**
 * login pages
 */
$app->mount('/', new LoginRoutes());

/**
 * Langs Routes
 */
$app->mount('/langs', new LangsRoutes());

/**
 * Langs Routes
 */
$app->mount('/rooms', new RoomsRoutes());