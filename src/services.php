<?php

use \App\Models\LangsModel;
use \App\Models\UserModel;

// User Models
$app['UsersModel'] = function ($app) {
	return new UserModel($app['db']);
};

$app['LangsModel'] = function ($app) {
	return new LangsModel($app['db']);
};

//register all services.
$app['RoomsModel'] = function ($app) {
	return new \App\Models\RoomsModel($app['db']);
};