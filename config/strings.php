<?php
$strings = [
	//login pages
	_('Sign in to start your session'),
	_('Email'),
	_('Password'),
	_('Sign In'),
	_('Sign Out'),

	//layout

	//menu
	_('Dashboard'),
	_('Main Navigation'),
	_('Languages'),
	_('Rooms'),
	_('Profile'),
	_('List'),

];